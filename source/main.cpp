/*
 *  SPDX-FileCopyrightText: 2020 Carson Black <uhhadd@gmail.com>
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#include <QApplication>
#include <QCoreApplication>
#include <QDebug>
#include <QIcon>
#include <QModelIndex>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include <KLocalizedContext>
#include <KLocalizedString>

#include "bloc.h"
#include "mpris.h"

auto main(int argc, char *argv[]) -> int
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    auto app = new QApplication(argc, argv);

    {
        QString mpris2Name("org.mpris.MediaPlayer2.org.kde.Kine");
        bool canRegister = QDBusConnection::sessionBus().registerService(mpris2Name);

        if (!canRegister)
            canRegister = QDBusConnection::sessionBus().registerService("org.mpris.MediaPlayer2.org.kde.Kine.instance" + QString::number(qApp->applicationPid()));
        
        if (canRegister) {
            new MediaPlayer2Adaptor(app);
            QDBusConnection::sessionBus().registerObject("/org/mpris/MediaPlayer2", app);
        }
    }

    KLocalizedString::setApplicationDomain("org.kde.Kine");

    QApplication::setOrganizationName("KDE");
    QApplication::setOrganizationDomain("org.kde");
    QApplication::setApplicationName("Kine");

    QApplication::setWindowIcon(QIcon::fromTheme(QString("org.kde.Kine")));
    QApplication::setDesktopFileName("org.kde.Kine.desktop");

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextObject(new KLocalizedContext(&engine));
    engine.rootContext()->setContextProperty("Bloc", new Bloc(nullptr));
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     app, [url](QObject *obj, const QUrl &objUrl) {
        if ((obj == nullptr) && url == objUrl) {
            QCoreApplication::exit(-1);
        }
    }, Qt::QueuedConnection);
    engine.load(url);

    return QApplication::exec();
}