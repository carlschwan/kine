#include "playlistmodel.h"

int PlaylistModel::rowCount(const QModelIndex& parent) const
{
    Q_UNUSED(parent)

    return tracks.count();
}

QVariant PlaylistModel::data(const QModelIndex& idx, int role) const
{
    if (!checkIndex(idx)) {
        return QVariant();
    }

    auto row = idx.row();

    switch (role) {
    case Qt::DisplayRole:
        return tracks[row].fileName();
    }

    return QVariant();
}

void PlaylistModel::enqueue(const QUrl& url)
{
    beginInsertRows(QModelIndex(), tracks.count(), tracks.count());
    tracks << url;
    endInsertRows();
    recomputeNextState();
}

void PlaylistModel::setPlaying(int currentlyPlaying)
{
    if (this->currentlyPlaying != currentlyPlaying) {
        this->currentlyPlaying = currentlyPlaying;
        Q_EMIT playingChanged();
        recomputeNextState();
    }
}

void PlaylistModel::recomputeNextState()
{
    if (currentlyPlaying == (tracks.length()-1)) {
        m_next = QUrl();
        m_canNext = false;
        m_nextIndex = -1;
        Q_EMIT nextChanged();
        return;
    }
    m_next = tracks[currentlyPlaying+1];
    m_canNext = true;
    m_nextIndex = currentlyPlaying + 1;
    Q_EMIT nextChanged();
}

bool PlaylistModel::canNext()
{
    return m_canNext;
}

QUrl PlaylistModel::next()
{
    return m_next;
}

int PlaylistModel::nextIndex()
{
    return m_nextIndex;
}
