/*
 *  SPDX-FileCopyrightText: 2020 Carson Black <uhhadd@gmail.com>
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

#pragma once

#include <QObject>
#include <QUrl>
#include <QVariant>
#include <QDebug>
#include <QFileDialog>
#include <QStandardPaths>

#include <KLocalizedString>

#include <playlistmodel.h>

#define Holder(type, name)\
    Q_PROPERTY(type name MEMBER m_ ## name NOTIFY name ## Changed)\
    type m_ ## name;\
    Q_SIGNAL void name ## Changed ();

class Bloc : public QObject {
    Q_OBJECT

public:
    Bloc(QObject *parent = nullptr)
        : QObject(parent)
        , m_video(QUrl())
        , m_shouldShowControls(false)
        , m_media(QVariant())
        , m_player(QVariant())
        , m_playlist(new PlaylistModel(this))
    {}

private:
    Q_PROPERTY(QUrl video MEMBER m_video NOTIFY videoChanged)
    Q_PROPERTY(QString videoFile READ videoFile NOTIFY videoChanged)
    QUrl m_video;
    Q_SIGNAL void videoChanged();

    Q_PROPERTY(bool shouldShowControls MEMBER m_shouldShowControls NOTIFY shouldShowControlsChanged)
    bool m_shouldShowControls;
    Q_SIGNAL void shouldShowControlsChanged();

    auto videoFile() -> QString {
        return m_video.fileName();
    }

    Q_PROPERTY(QVariant media MEMBER m_media NOTIFY mediaChanged)
    QVariant m_media;
    Q_SIGNAL void mediaChanged();

    Q_PROPERTY(QVariant player MEMBER m_player NOTIFY playerChanged)
    QVariant m_player;
    Q_SIGNAL void playerChanged();

    Q_PROPERTY(QVariant drawer MEMBER m_drawer NOTIFY drawerChanged)
    QVariant m_drawer;
    Q_SIGNAL void drawerChanged();

    Q_PROPERTY(PlaylistModel* playlist MEMBER m_playlist NOTIFY playlistChanged)
    PlaylistModel* m_playlist;
    Q_SIGNAL void playlistChanged();

    Holder(QVariant, openDialog)
};