#pragma once

#include <QAbstractListModel>
#include <QDebug>
#include <QUrl>

class PlaylistModel : public QAbstractListModel
{
    Q_OBJECT

    Q_PROPERTY(QUrl next READ next NOTIFY nextChanged)
    Q_PROPERTY(bool canNext READ canNext NOTIFY nextChanged)
    Q_PROPERTY(int nextIndex READ nextIndex NOTIFY nextChanged)
    Q_PROPERTY(int playing READ playing WRITE setPlaying NOTIFY playingChanged)

public:
    PlaylistModel(QObject* parent) : QAbstractListModel(parent)
    {
    }

private:
    QList<QUrl> tracks;
    QUrl m_next = QUrl();
    bool m_canNext = false;
    int m_nextIndex = -1;
    int currentlyPlaying = -1;
    void recomputeNextState();

public:
    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    QVariant data(const QModelIndex& idx, int role = Qt::DisplayRole) const override;

    Q_INVOKABLE void enqueue(const QUrl& url);

    Q_INVOKABLE void setPlaying(int idx);
    int playing() { return currentlyPlaying; }
    Q_SIGNAL void playingChanged();

    bool canNext();
    QUrl next();
    int nextIndex();
    Q_SIGNAL void nextChanged();
};
