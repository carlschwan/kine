import QtQuick 2.15
import QtQuick.Controls 2.15 as QQC2
import QtQuick.Layouts 1.10
import org.kde.kirigami 2.12 as Kirigami
import QtQuick.Dialogs 1.0

Kirigami.GlobalDrawer {
    width: Math.min(Math.max(0.33 * applicationWindow().width, Kirigami.Units.gridUnit * 20), applicationWindow().width)
    height: parent.height

    Component.onCompleted: Bloc.drawer = this

    FileDialog {
        folder: shortcuts.movies
        onAccepted: {
            Bloc.playlist.enqueue(fileUrl)
        }
        Component.onCompleted: Bloc.dialog = this
    }

    ColumnLayout {
        anchors.fill: parent
        anchors.centerIn: parent

        ListView {
            model: Bloc.playlist

            delegate: Kirigami.BasicListItem {
                required property string display
                required property int index
                bold: index == Bloc.playlist.playing
                text: display
            }

            Layout.fillHeight: true
            Layout.fillWidth: true

            Kirigami.PlaceholderMessage {
                text: i18n("You don't have any videos in your playlist. Why not play some?")

                visible: parent.count === 0

                anchors.centerIn: parent
                width: parent.width - (Kirigami.Units.largeSpacing * 4)
            }
        }

        QQC2.ToolBar {
            position: QQC2.ToolBar.Footer
            Layout.fillWidth: true

            QQC2.ToolButton {
                text: i18n("Open Video...")
                icon.name: "document-open"

                anchors {
                    right: parent.right
                }

                onClicked: Bloc.dialog.open()
            }
        }
    }
}