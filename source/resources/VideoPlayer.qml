/*
 *  SPDX-FileCopyrightText: 2020 Carson Black <uhhadd@gmail.com>
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

import QtQuick 2.10
import QtMultimedia 5.12 as Multimedia

Item {
    anchors.fill: parent
    Multimedia.VideoOutput {
        anchors.fill: parent
        source: Multimedia.MediaPlayer {
            autoPlay: true
            source: Bloc.video
            property Binding mediaBinder: Binding {
                target: Bloc
                property: "media"
                value: {}
            }
            property Binding playerBinder: Binding {
                target: Bloc
                property: "player"
                value: {}
            }
            property Connections playlistManager: Connections {
                target: Bloc.playlist
                function onNextChanged() {
                    if (Bloc.playlist.canNext) {
                        if (Bloc.video == "") {
                            Bloc.video = Bloc.playlist.next
                            Bloc.playlist.setPlaying(Bloc.playlist.nextIndex)
                        }
                    }
                }
            }
            Component.onCompleted: {
                mediaBinder.value = this.metaData
                playerBinder.value = this
            }
            onPlaybackStateChanged: {
                if (position === duration && playbackState === 0) {
                    if (Bloc.playlist.canNext) {
                        Bloc.video = Bloc.playlist.next
                        Bloc.playlist.setPlaying(Bloc.playlist.nextIndex)
                    }
                }
            }
        }
    }
}