/*
 *  SPDX-FileCopyrightText: 2020 Carson Black <uhhadd@gmail.com>
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

import QtQuick 2.0
import org.kde.kirigami 2.12 as Kirigami

Kirigami.RouterWindow {
    initialRoute: "video"
    controlsVisible: false
    pageStack.globalToolBar.style: Kirigami.ApplicationHeaderStyle.None

    property Drawer drawer: Drawer {}

    Kirigami.PageRoute {
        name: "video"
        PlayerPage {}
    }
}