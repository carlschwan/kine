/*
 *  SPDX-FileCopyrightText: 2020 Carson Black <uhhadd@gmail.com>
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

import QtQuick 2.10

Item {
    anchors.fill: parent
    function tabbed() {
        Bloc.shouldShowControls = true
        children[0].inactive.restart()
    }
    MouseArea {
        anchors.fill: parent
        hoverEnabled: true
        propagateComposedEvents: true

        onEntered: {
            Bloc.shouldShowControls = true
            exit.stop()
            inactive.restart()
        }
        onPositionChanged: {
            Bloc.shouldShowControls = true
            inactive.restart()
        }
        onExited: {
            exit.restart()
            inactive.stop()
        }

        property Timer exit: Timer {
            interval: 400
            onTriggered: Bloc.shouldShowControls = false
        }
        property Timer inactive: Timer {
            interval: 5000
            onTriggered: Bloc.shouldShowControls = false
        }
    }
    Connections {
        target: Bloc
        function shouldShowControlsChanged() {
            parent.tabbed()
        }
    }
}