/*
 *  SPDX-FileCopyrightText: 2020 Carson Black <uhhadd@gmail.com>
 *
 *  SPDX-License-Identifier: GPL-3.0-or-later
 */

import QtQuick 2.0
import org.kde.kirigami 2.12 as Kirigami

Kirigami.Page {
    topPadding: 0
    leftPadding: 0
    rightPadding: 0
    bottomPadding: 0
    Rectangle { anchors.fill: parent; color: "black" }
    VideoPlayer {}
    ControlHoverManager {}
    FocusScope {
        anchors.fill: parent
        opacity: Bloc.shouldShowControls ? 1 : 0
        Behavior on opacity {
            NumberAnimation {
                duration: 200
            }
        }
        VideoControls {}
        VideoData {}
    }
    VideoDropAccepter {}
}